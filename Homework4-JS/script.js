function filterBy(arr,data_type) {
    let new_array=[];
    for (let item of arr){
        if (typeof(item)!==data_type ){
            new_array.push(item);
        }
    }
    return new_array;
}

let result = filterBy(['hello', 'world', 23 , '23', null],'string');
console.log(result);
