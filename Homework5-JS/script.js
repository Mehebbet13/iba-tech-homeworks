let header = document.getElementById('head');
let list = header.getElementsByClassName("tabs-title");
console.log(list);
let content = document.getElementById('tabs-content');
let text = Array.from(content.getElementsByClassName("texts"));
console.log(text);
function hide(idx){
    for (let i = 0; i < text.length; i++) {
        console.log(text[i]);
        if (i!==idx) {
            text[i].style.display = 'none';
        }
        else{
            text[i].style.display = 'block';
        }
    }
}
hide(0);
for (let i = 0; i < list.length; i++) {
    console.log(list[i]);
    list[i].addEventListener("click", function () {
        let current = document.getElementsByClassName('active');
        current[0].className = current[0].className.replace('active', "");
        this.className += " active";
        hide(i);

    });
}

